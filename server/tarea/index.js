#!/usr/bin/node

const debug = false;
const sqlite3 = require('sqlite3').verbose();
const { open } = require('sqlite');
const httpcoolmule = require('./httpcoolmule');
const tableName = 'task';
const port = process.env.TAREA_PORT || 42956; // H A Z L O

var database;

let prepareDatabasePromise;

function createTask() {
    return database.run(`
        INSERT INTO ${tableName} (text, due, lasts)
        VALUES ('Neue Aufgabe', null, 7)
    `)
    .then(err => {return Promise.resolve(this.lastID);});
}

function deleteTask(taskId) {
    debug && console.log('deleteTask', taskId);

    return database.run(
        `DELETE FROM  ${tableName} WHERE task_id = ?`, taskId
    )
    .then(result => Promise.resolve({}));
}

function get(all) {
    debug && console.log('get', all);

    return prepareDatabase().then(queryTasks.bind(this, all));
}

function getCreate(parameters) {
    return prepareDatabase().then(createTask).then(queryTasks);
}

function prepareDatabase() {
    debug && console.log('prepareDatabase', prepareDatabasePromise);

    return prepareDatabasePromise = prepareDatabasePromise || database.exec(`
        CREATE TABLE IF NOT EXISTS ${tableName}
        (
            task_id INTEGER PRIMARY KEY AUTOINCREMENT,
            text text NOT NULL,
            lasts int NOT NULL,
            due date
        )
    `);
}

function put(config) {
    debug && console.log('put', config);

    return prepareDatabase()
        .then(updateTask.bind(this, config.task))
        .then(queryTasks.bind(this, 'true' === config.all));
}

function queryTasks() {
    debug && console.log('queryTasks');

    return database.all(`
        SELECT task_id, text, date(coalesce(due, '1970-01-01'), lasts || ' days') AS due, lasts
        FROM ${tableName}
    `);
}

function updateTask(task) {
    debug && console.log('updateTask', task);

    return database.run(
      `UPDATE ${tableName} SET text = ?, lasts = ?, due = date(?, '-' || ? || ' days') WHERE task_id = ?`,
      task.text,
      task.lasts,
      task.due,
      task.lasts,
      task.task_id
    );
}

open({
    filename: 'tarea.db',
    driver: sqlite3.Database
}).then((db) => {
    database = db;

    httpcoolmule.start({
        origin: 'https://tarea.coolmule.de',
        referers: [
          'http://localhost:4200/',
          'http://localhost:42956/',
          'http://tarea.coolmule.de/',
          'https://tarea.coolmule.de/'
        ],
        port,
        get: (parameters) => {
            return get(parameters.all === 'true')
        },
        getCreate,
        put: (putEvent) => {
            return put(JSON.parse(putEvent));
        },
        delete: (parameters) => {
            return deleteTask(parameters.taskId)
        }
    });
});
