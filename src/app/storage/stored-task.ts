export interface StoredTask {
    task_id: number,
    text: string,
    lasts: number,
    due: string
}
