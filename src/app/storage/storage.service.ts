import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { Task } from '../task';
import { StoredTask } from './stored-task';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  // readonly URL_BASE = 'https://localhost:42956/';
  readonly URL_BASE = 'https://coolmule.de:42956/';
  readonly URL_CREATE = this.URL_BASE + 'create';

  private readonly mock = 'tarea.coolmule.de' !== window.location.hostname;
  private nextId = 0;
  private mockedTasks = this.mock ? [
    {task_id: this.nextId++, text: 'Aufgabe1', lasts: 90, due: '2022-01-01'} as StoredTask,
    {task_id: this.nextId++, text: 'Aufgabe2', lasts: 180, due: '2022-04-01'} as StoredTask,
    {task_id: this.nextId++, text: 'Aufgabe3', lasts: 360, due: '2023-01-01'} as StoredTask,
    {task_id: this.nextId++, text: 'Aufgabe4', lasts: 360, due: '2022-07-01'} as StoredTask,
  ] : [];

  constructor(private http: HttpClient) { }

  deleteTask(taskId: number): Observable<{success: boolean}> {
    console.log('deleteTask', taskId);
    if (this.mock) {
      this.mockedTasks = this.mockedTasks.filter(task => task.task_id !== taskId);
      return of({success: true});
    }

    return this.http.delete(this.URL_BASE, {params: {taskId}})
      .pipe(take(1))
      .pipe(map((result: any) => {return {success: true};}));
  }

  loadTasks(): Observable<StoredTask[]> {
    if (this.mock) {
      return of(this.mockedTasks);
    }

    return this.http.get<StoredTask[]>(this.URL_BASE);
  }

  storeTask(task: Task): Observable<{success: boolean}> {
    return (
      this.mock ?
        of(this.mockedTasks) : this.http.put<StoredTask[]>(this.URL_BASE, {...task, due: this.toIso(task.due)})
    )
      .pipe(take(1))
      .pipe(map((result: StoredTask[]) => {return {success: true};}));
  }

  createTask(): Observable<StoredTask[]> {
    if (this.mock) {
      const today = new Date();
      const todayIso = this.toIso(today);
      const newTask =
        {task_id: this.nextId++, text: 'Neue Aufgabe ' + todayIso, lasts: this.nextId * 10, due: todayIso};
      this.mockedTasks.push(newTask);
      return of(this.mockedTasks);
    }
    return this.http.get<StoredTask[]>(this.URL_CREATE);
  }

  toIso(date: Date): string {
    return [date.getFullYear(), this.twoDigit(date.getMonth() + 1), this.twoDigit(date.getDate())].join('-');
  }

  twoDigit(i: number): string {
    return (100 + i + '').substring(1);
  }

}
