import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Task } from 'src/app/task';

@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html',
  styleUrls: ['./tarea.component.css']
})
export class TareaComponent implements OnInit {

  @Input() task: Task | null = null;

  @Output() save = new EventEmitter<Task>();
  @Output() delete = new EventEmitter<number>();
  
  editMode = false;

  private oldTask: Task | null = null;

  readonly JSON: {stringify: Function};

  constructor() {
    this.JSON = JSON;
  }

  ngOnInit(): void {}

  onChangeDue(event: any): void {
    // console.log('onChangeDue', event.target.value);
    this.task = {...this.task, due: new Date(event.target.value)} as Task;
  }

  onChangeLasts(event: any): void {
    // console.log('onChangeLasts');
    this.task = {...this.task, lasts: event.target.value} as Task;
  }

  onChangeText(event: any): void {
    // console.log('onChangeText');
    this.task = {...this.task, text: event.target.value} as Task;
  }

  onClickCancel(): void {
    this.task = this.oldTask;
    this.stopEditing();
  }

  onClickDelete(): void {
    // console.log('onClickDelete');
    let newDue = new Date();
    newDue.setDate(newDue.getDate() + this.task!.lasts);
    this.task = {...this.task, due: newDue} as Task;
    this.emitDeleteEvent();
  }

  onClickDone(): void {
    // console.log('onClickDone');
    let newDue = new Date();
    newDue.setDate(newDue.getDate() + this.task!.lasts);
    this.task = {...this.task, due: newDue} as Task;
    this.emitSaveEvent();
   }
 
  onClickEdit(): void {
    this.startEditing();
  }

  onClickReset(): void {
    this.task = this.oldTask;
  }

  onClickSave(): void {
    this.stopEditing();
    this.emitSaveEvent();
  }

  emitDeleteEvent(): void {
    // console.log('emitDeleteEvent');
    this.delete.emit(this.task!.task_id);
  }

  emitSaveEvent(): void {
    // console.log('emitSaveEvent');
    this.save.emit(this.task!);
  }

  formatDate(date: Date): string {
    return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
  }

  formatDateIso(date: Date): string {
    // console.log('formatDateIso', date);
    return date.getFullYear() + '-' + (date.getMonth() + 101 + '').substring(1) + '-' + (date.getDate() + 100 + '').substring(1);
  }

  startEditing(): void {
    this.oldTask = this.task;
    this.editMode = true;
  }

  stopEditing(): void {
    this.editMode = false;
  }

}
