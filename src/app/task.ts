export interface Task {
    task_id: number,
    text: string,
    lasts: number,
    due: Date,
    dirty?: boolean,
}
