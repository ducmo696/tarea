import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Observable, of, Subscription, zip } from 'rxjs';
import { take, timeout } from 'rxjs/operators';
import { StorageService } from './storage/storage.service';
import { Task } from './task';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'tarea';

  readonly JSON: {stringify: Function};

  loadedAufgaben: Task[] = [];

  private readonly subscriptions = new Subscription();

  showAllTasks: boolean = false;

  filterText = '';

  constructor(private storage: StorageService) {
    this.JSON = JSON;
  }

  findDueTasks(): Observable<Task[]> {
    return of(
      (
        this.showAllTasks
          ? this.loadedAufgaben.filter(task => task.text.toLowerCase().indexOf(this.filterText.toLowerCase()) >= 0)
          : this.loadedAufgaben.filter(task => new Date().getTime() >= task.due.getTime())
      ).sort((task1: Task, task2: Task) => task1!.due.getTime() - task2!.due.getTime())
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  ngOnInit(): void {
    this.subscriptions.add(this.storage.loadTasks().pipe(take(1)).subscribe(
      tasks => this.loadedAufgaben = tasks.map(task => {
        return {...task, due: new Date(task.due)} as Task; // Konvertierung hätte ich lieber im Storage-Service
      })
    ));
  }

  onChangeFilter(event: any): void {
    // console.log('onChangeFilter', event.target.value);
    this.filterText = event.target.value;
  }

  onChangeShowAllTasks(): void {
    this.showAllTasks = !this.showAllTasks;
  }

  onClickCreate(): void {
    this.subscriptions.add(this.storage.createTask().subscribe(
      allTasks => this.loadedAufgaben = allTasks.map(task => {
        return {...task, due: new Date(task.due)} as Task;
      })
    ));
  }

  onTaskDelete(taskId: number): void {
    // console.log('onTaskDelete', taskId);
    this.subscriptions.add(
      this.storage.deleteTask(taskId).subscribe(
      result => {
        // console.log('onTaskDelete result', result);
        if (result.success) {
          this.loadedAufgaben = this.loadedAufgaben.filter(
            task => task.task_id !== taskId
          );
        }
      }
    ));
  }

  onTaskSave(savedTask: Task): void {
    // console.log('onTaskSave', savedTask);
    this.loadedAufgaben = this.loadedAufgaben.map(
      task => task.task_id === savedTask.task_id ? savedTask : task
    );
    savedTask.dirty = true;
    this.subscriptions.add(
      zip(this.storage.storeTask(savedTask), interval(1000))
        .subscribe(
          ([result]) => {
            if (result.success) {
              delete savedTask.dirty;
            }
          }
        )
    );
  }

}
